const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require ('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const autoprefixer = require ('autoprefixer');
const postcssSorting = require ('postcss-sorting');
const cssMqPacker = require ('css-mqpacker');
const OptimizeCssAssetsWebpackPlugin = require ('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

let conf = {
    entry: {
        main: './src/scripts/main.js'
    },
    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, 'assets')
    },
    devServer: {
        overlay: true,
        contentBase: './src',
        watchContentBase: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.s?css$/,
                exclude: /(node_modules)/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 3,
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    autoprefixer,
                                    postcssSorting,
                                    cssMqPacker,
                                ],
                                sourceMap: true
                            }
                        },
                        'resolve-url-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                outputStyle: 'compact',
                                sourceMap: true,
                                sourceComments: true
                            }
                        }
                    ]
                }),
            },
            {
                test: /\.html$/,
                use: [ {
                    loader: 'html-loader',
                    options: {
                        minimize: false
                    }
                }]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                include: path.resolve(__dirname, 'src/fonts'),
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name]/[name].[ext]',
                        publicPath: '../fonts',
                        outputPath: './fonts/'
                    }
                }]
            },
        ]
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: '[file].map'
        }),
        new ExtractTextPlugin({
            filename: 'css/[name].css'
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
        }),
        new CleanWebpackPlugin([
            'assets'
        ]),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ],
    optimization: {
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: false, // Must be set to true if using source-maps in production
            }),
            new OptimizeCssAssetsWebpackPlugin({})
        ],
    },
};

module.exports = (evn, options) => {
    let production = options.mode === 'production';

    conf.devtool = production
        ? false
        : 'source-map';

    return conf;
};
